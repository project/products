<?php

namespace Drupal\products\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Products entities.
 *
 * @ingroup products
 */
class ProductsDeleteForm extends ContentEntityDeleteForm {


}
